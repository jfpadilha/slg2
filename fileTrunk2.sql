
CREATE TABLE trunk(
codeTrunk INT NOT NULL,
nameTrunk VARCHAR(40),
descTrunk BOOLEAN
PRIMARY KEY (codeTrunk));

/*
Texto adicionado por último
*/

-- TUDO ISTO ABAIXO FOI ADD SOMENTE NA TRUNK2


-- Criar uma consulta que mostre os projetos e as fases de cada projeto. Exibir o id e nome do projeto e o nome de cada fase que compõe o projeto. 
--Considerar somente os projetos cujo tipo é igual a Projeto especial.
SELECT a.id, a.nome, c.nome
FROM projeto a, faseprojeto b, fase c
WHERE a.id = b.idproj
	AND b.codfase = c.codfase
	AND a.tipo = 'Projeto especial'
GROUP BY a.id, a.nome, c.nome
------------------------------ | | --------------------------------------
-- Consulta para mostrar a lista dos participantes das atividades que possuem sequencia maior do que 5. 
--Exibir o nome da pessoa participante, o nome da função, o id do projeto e a sequencia da atividade.
SELECT c.nome, d.nome, a.idproj, a.sequencia
FROM participanteativ a, atividade b, pessoa c, funcao d
WHERE a.sequencia > '5'
	AND b.sequencia > '5'
	AND c.codpess = a.codpess
	AND a.codfunc = d.codfunc
	AND a.idproj = b.idproj
GROUP BY c.nome, d.nome, a.idproj, a.sequencia

------------------------------ | | --------------------------------------
-- Lista de atividades e a suas ocorrências. 
--Data fim real no período de 01/01/12 a 31/12/13. 
--Mostrar nome, data fim real, data, hora e descrição da ocorrência. Ordenar por data e hora da ocorrência.
SELECT a.nome, a.datafimreal, b.dataocor, b.horaocor, b.descricao
FROM atividade a, ocorrenciaativ b
WHERE a.idproj = b.idproj
	AND a.sequencia = b.sequencia
ORDER BY b.dataocor, b.horaocor
------------------------------ | | --------------------------------------
-- Lista dos clientes e as pessoas vinculadas a eles. 
--Nome e endereço do cliente, nome e endereço da pessoa. 
--Todos os clientes devem ser mostrado
SELECT a.nome, a.endereco, b.nome, b.endereco
FROM cliente a, pessoa b
WHERE a.codcli = b.codcli
GROUP BY a.nome, a.endereco, b.nome, b.endereco
ORDER BY a.nome